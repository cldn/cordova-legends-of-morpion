////////////////////////////////////////////////////////////////////////////////
// On définit un objet controleur qui va contenir les controleurs de nos pages
////////////////////////////////////////////////////////////////////////////////

var controleur = {};

////////////////////////////////////////////////////////////////////////////////
// Session : variables qui représentent l'état de l'application
////////////////////////////////////////////////////////////////////////////////

controleur.session = {
    partieEnCours: null, // La partie en train d'être jouée
};

////////////////////////////////////////////////////////////////////////////////
// initialise : exécuté au démarrage de l'application (voir fichier index.js)
////////////////////////////////////////////////////////////////////////////////

controleur.init = function () {
    // On duplique Header et Footer sur chaque page (sauf la première !)
    $('div[data-role="page"]').each(function (i) {
        if (i > 0)
            $(this).html($('#morpionHeader').html() + $(this).html() + $('#morpionFooter').html());
    });
    // On afficher la page d'accueil
    $.mobile.changePage("#vueAccueil");
};

////////////////////////////////////////////////////////////////////////////////
// Controleurs de pages : 1 contrôleur par page, qui porte le nom de la page
//  et contient les callbacks des événements associés à cette page
////////////////////////////////////////////////////////////////////////////////

controleur.vueAccueil = {
    init: function () {
        $("#nomJoueur1").val("");
        $("#nomJoueur2").val("");

        const joueursExistants = modele.dao.getJoueurs();
        // Vider les select, leur rendre le select par défaut, et rajouter les noms de joueurs disponibles
        $("#choixJoueur1").empty();
        $("#choixJoueur2").empty();
        $("#choixJoueur1").append(`<option selected value=""></option>`);
        $("#choixJoueur2").append(`<option selected value=""></option>`);
        for (const entry of joueursExistants) {
            $("#choixJoueur1").append(`
                <option value="${entry.nom}">
                    ${entry.nom}
                </option>
            `);
            $("#choixJoueur2").append(`
                <option value="${entry.nom}">
                    ${entry.nom}
                </option>
            `);
        }
    },

    nouvellePartie: function () {
        // Valeurs des champs "nouveau joueur"
        var nomJoueur1 = $("#nomJoueur1").val();
        var nomJoueur2 = $("#nomJoueur2").val();
        var imgJoueur1 = $("#imgJoueur1").val();
        var imgJoueur2 = $("#imgJoueur2").val();

        // Valeurs des champs "sélectionner un joueur existant"
        let choixJoueur1 = $("#choixJoueur1").val();
        let choixJoueur2 = $("#choixJoueur2").val();

        if (choixJoueur1 !== "" && choixJoueur2 !== "") {
            // Transformer les "choixJoueur1" qui sont juste les noms des joueurs en objets Joueur
            choixJoueur1 = modele.dao.getJoueur(choixJoueur1);
            choixJoueur2 = modele.dao.getJoueur(choixJoueur2);
            if (choixJoueur1.nom === choixJoueur2.nom) {
                alert("Sélectionnez deux joueurs différents pour jouer !");
            } else {
                // Si il y a des joueurs sélectionnés, on choisit leur configuration
                nomJoueur1 = choixJoueur1.nom;
                nomJoueur2 = choixJoueur2.nom;
                imgJoueur1 = choixJoueur1.img;
                imgJoueur2 = choixJoueur2.img;
            }
        }

        if (nomJoueur1 === "") {
            alert("Veuillez compléter le nom du joueur 1");
        } else if (nomJoueur2 === "") {
            alert("Veuillez compléter le nom du joueur 2");
        } else if (imgJoueur1 === "") {
            alert("Veuillez ajouter une image pour le joueur 1");
        } else if (imgJoueur2 === "") {
            alert("Veuillez ajouter une image pour le joueur 2");
        }
        else {
            // Stockage du joueur courant dans la session (le joueur 1 commencera)
            controleur.session.joueurCourant = nomJoueur1;
            // On utilise le modèle pour créer une nouvelle partie
            controleur.session.partieEnCours = new modele.Partie(nomJoueur1, imgJoueur1, nomJoueur2, imgJoueur2);
            // On "propage" le nom du premier joueur
            $('span[data-role="nomJoueur"]').each(function () {
                $(this).html(controleur.session.partieEnCours.joueurCourant.nom);
            });
            // On "propage" les caractéristiques du joueur 1 sur toutes les vues concernées
            $('span[data-role="nomJoueur1"]').each(function () {
                $(this).html(nomJoueur1);
            });
            $('img[data-role="imgJoueur1"]').each(function () {
                $(this).attr('src', imgJoueur1);
            });
            // On "propage" les caractéristiques du joueur 2 sur toutes les vues concernées
            $('span[data-role="nomJoueur2"]').each(function () {
                $(this).html(nomJoueur2);
            });
            $('img[data-role="imgJoueur2"]').each(function () {
                $(this).attr('src', imgJoueur2);
            });
            // Et on passe à une autre vue
            $.mobile.changePage("#vueJeu");
        }
    },

    takePicture: function (EltId) {
        // Appel méthode du modèle permettant de prendre une photo
        // en lui passant en paramètre successCB et errorCB
        modele.takePicture(
            // successCB : on met à jour la vue (champ cameraImage)
            function (uneImage) {
                // on récupère un objet Image
                $(`#${EltId}`).attr("value", uneImage);
            },
            // erreurCB : on affiche un message approprié
            function () {
                console.log("Impossible de prendre une photo");
            }
        );
    }
};
// On définit ici la callback exécutée au chargement de la vue Accueil
$(document).on("pagebeforeshow", "#vueAccueil", function () {
    controleur.vueAccueil.init();
});

////////////////////////////////////////////////////////////////////////////////
controleur.vueJeu = {

    init: function () {
        // on cache la div resultat
        $("#resultat").hide();
    },

    jouer: function (posX, posY) {
        // on interroge le modèle pour voir le résultat du nouveau coup
        const symbole = controleur.session.partieEnCours.play(posX, posY);
        $(`#${posX}-${posY}`).html(symbole);
        // console.log($(`${posX}-${posY} `).text);
        // on désactive le bouton cliqué
        $(`#${posX}-${posY}`).prop('disabled', true);
        // on affiche le résultat
        if (controleur.session.partieEnCours.isFinished()) {
            const vainqueur = controleur.session.partieEnCours.getVainqueur();
            controleur.session.partieEnCours.end();
            controleur.vueJeu.freezePlateau();
            if (vainqueur === undefined) {
                $("#texteResultat").html(`Match nul !`).css("color", 'yellow');
            } else {

                $("#texteResultat").html(`${vainqueur.nom} remporte la partie !`).css("color", 'green');
            }
            $("#resultat").show();
        } else {
            $('span[data-role="nomJoueur"]').each(function () {
                $(this).html(controleur.session.partieEnCours.joueurCourant.nom);
            });
        }
    },

    freezePlateau: function () {
        for (let i = 1; i <= 3; i++) {
            for (let j = 1; j <= 3; j++) {
                $(`#${i}-${j}`).prop('disabled', true);
            }
        }
    },

    cleanPlateau: function () {
        for (let i = 1; i <= 3; i++) {
            for (let j = 1; j <= 3; j++) {
                $(`#${i}-${j}`).prop('disabled', false);
                $(`#${i}-${j}`).html("");
            }
        }
    },

    rejouer: function () {
        controleur.vueJeu.init();
        controleur.session.partieEnCours.reset();
        controleur.vueJeu.cleanPlateau();
    },

    finPartie: function () {
        $.mobile.changePage("#vueFin");
    }
};

// On définit ici la callback exécutée au chargement de la vue Jeu
$(document).on("pagebeforeshow", "#vueJeu", function () {
    controleur.vueJeu.init();
});

////////////////////////////////////////////////////////////////////////////////
controleur.vueFin = {
    init: function () {
        const data = controleur.session.partieEnCours.getStats();
        const stats = {
            winsJ1: data[0],
            winsJ2: data[1],
            nbParties: data[2],
            lossesJ1: data[1],
            lossesJ2: data[0],
            draws: data[2] - data[0] - data[1]
        }
        $("#nbVictoiresJ1").html(stats.winsJ1);
        $("#nbNulsJ1").html(stats.draws);
        $("#nbDefaitesJ1").html(stats.lossesJ1);
        $("#nbVictoiresJ2").html(stats.winsJ2);
        $("#nbNulsJ2").html(stats.draws);
        $("#nbDefaitesJ2").html(stats.lossesJ2);
    },

    retourJeu: function () {
        controleur.vueJeu.rejouer();
        $.mobile.changePage("#vueJeu");
    },

    retourAccueil: function () {
        controleur.vueJeu.cleanPlateau();
        $.mobile.changePage("#vueAccueil");
    }
};

// On définit ici la callback exécutée au chargement de la vue Fin
$(document).on("pagebeforeshow", "#vueFin", function () {
    controleur.vueFin.init();
});

controleur.vueScores = {
    init: function () {
        const resultats = modele.dao.getJoueurs();
        $("#scores").empty();
        $("#scores").append(`
            <tr>
                <th>Pseudo</th>
                <th>Victoires</th>
                <th>Image</th>
            </tr>
        `);
        for (const resultat of resultats) {
            $("#scores").append(`
                <tr>
                    <td><strong>${resultat.nom}</strong></td>
                    <td><strong>${resultat.score}</strong></td>
                    <td><img class="profile" src="${resultat.img}" /></td>
                </tr>
            `);
        }
    },

    voirScores: function () {
        $.mobile.changePage("#vueScores");
    }
}

// On définit ici la callback exécutée au chargement de la vue Scores
$(document).on("pagebeforeshow", "#vueScores", function () {
    controleur.vueScores.init();
});
