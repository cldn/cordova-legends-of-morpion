var modele = {};

modele.dao = {
    // sauvegarde la partie au format JSON dans le storage
    saveJoueurs: function(partie) {
        modele.dao.saveJoueur(partie.joueur1);
        modele.dao.saveJoueur(partie.joueur2);
    },
    saveJoueur: function(joueur) {
        const currentJoueur = modele.dao.getJoueur(joueur.nom);
        if (currentJoueur === null) {
            window.localStorage.setItem(joueur.nom, `${joueur.score}|${joueur.img}`);
        } else {
            // On écrase l'ancienne image avec l'image courante
            window.localStorage.setItem(joueur.nom, `${+joueur.score + +currentJoueur.score}|${joueur.img}`);
        }
    },
    getJoueurs: function() {
        const classement = [];
        for (i = 0; i < window.localStorage.length; i++) {
            const key = window.localStorage.key(i);
            const parsedInfos = window.localStorage.getItem(key).split('|');
            classement.push(new modele.Joueur(key, parsedInfos[1], parsedInfos[0]));
        }
        return classement;
    },
    // charge la partie d'un joueur, si elle existe, depuis le storage
    getJoueur: function(nomJoueur) {
        const joueur = window.localStorage.getItem(nomJoueur);
        if (joueur === null) {
            return null;
        } else {
            const parsedInfos = joueur.split('|');
            return new modele.Joueur(nomJoueur, parsedInfos[1], parsedInfos[0]);
        }
    }
}

modele.Joueur = function (nomJoueur, imgJoueur, scoreJoueur = 0) {
    this.nom = nomJoueur;
    this.img = imgJoueur;
    this.score = scoreJoueur;
}

// Le modele contient ici une seule classe : Partie
modele.Partie = function (nomJoueur1, imgJoueur1, nomJoueur2, imgJoueur2) {

    this.nbParties = 0;
    this.joueur1 = new modele.Joueur(nomJoueur1, imgJoueur1, 0);
    this.joueur2 = new modele.Joueur(nomJoueur2, imgJoueur2, 0);
    this.setup();
}

// constantes de classe
modele.Partie.SYMBOLE_J1 = 'X';
modele.Partie.SYMBOLE_J2 = 'O';

// Méthodes
modele.Partie.prototype = {

    setup: function () {
        this.grille = new Array(
            new Array(" ", " ", " "),
            new Array(" ", " ", " "),
            new Array(" ", " ", " ")
        );
        this.joueurCourant = this.joueur1;
    },

    play: function (posX, posY) {
        const symbole = (this.joueurCourant === this.joueur1 ? modele.Partie.SYMBOLE_J1 : modele.Partie.SYMBOLE_J2);
        this.grille[posX - 1][posY - 1] = symbole;
        this.joueurCourant = (this.joueurCourant === this.joueur1 ? this.joueur2 : this.joueur1);
        return symbole;
    },

    isFinished: function () {
        return (this.getVainqueur() === undefined ? this.boardIsFull() : true);
    },

    boardIsFull: function () {
        for (line of this.grille) {
            for (square of line) {
                if (square === " ") {
                    return false;
                }
            }
        }
        return true;
    },

    getVainqueur: function () {
        // shorter variable names to avoid an unreadable condition
        const g = this.grille;
        const j1 = modele.Partie.SYMBOLE_J1;
        const j2 = modele.Partie.SYMBOLE_J2;
        if (
            (g[0][0] === j1 && g[1][0] === j1 && g[2][0] === j1)
            || (g[0][1] === j1 && g[1][1] === j1 && g[2][1] === j1)
            || (g[0][2] === j1 && g[1][2] === j1 && g[2][2] === j1)
            || (g[0][0] === j1 && g[0][1] === j1 && g[0][2] === j1)
            || (g[1][0] === j1 && g[1][1] === j1 && g[1][2] === j1)
            || (g[2][0] === j1 && g[2][1] === j1 && g[2][2] === j1)
            || (g[0][0] === j1 && g[1][1] === j1 && g[2][2] === j1)
            || (g[0][2] === j1 && g[1][1] === j1 && g[2][0] === j1)
        ) {
            return this.joueur1;
        } else if (
            (g[0][0] === j2 && g[1][0] === j2 && g[2][0] === j2)
            || (g[0][1] === j2 && g[1][1] === j2 && g[2][1] === j2)
            || (g[0][2] === j2 && g[1][2] === j2 && g[2][2] === j2)
            || (g[0][0] === j2 && g[0][1] === j2 && g[0][2] === j2)
            || (g[1][0] === j2 && g[1][1] === j2 && g[1][2] === j2)
            || (g[2][0] === j2 && g[2][1] === j2 && g[2][2] === j2)
            || (g[0][0] === j2 && g[1][1] === j2 && g[2][2] === j2)
            || (g[0][2] === j2 && g[1][1] === j2 && g[2][0] === j2)
        ) {
            return this.joueur2;
        } else {
            return undefined;
        }
    },

    getStats: function () {
        return [this.joueur1.score, this.joueur2.score, this.nbParties];
    },

    end: function () {
        // Establish the score and stats
        this.nbParties++;
        switch (this.getVainqueur()) {
            case this.joueur1:
                this.joueur1.score++;
                break;
            case this.joueur2:
                this.joueur2.score++;
            default:
                break;
        }
        modele.dao.saveJoueurs(this);
    },

    reset: function () {
        // Reset the board
        this.setup();
    }

};


////////////////////////////////////////////////////////////////////////////////
// Méthode pour capturer une image avec le téléphone encodée en Base64
////////////////////////////////////////////////////////////////////////////////
modele.takePicture = function (successCB, errorCB) {
    navigator.camera.getPicture(
        function (imageData) {
            // imageData contient l'image capturée au format Base64, sans en-tête MIME
            // On appelle successCB en lui transmettant une entité Image
            successCB.call(this, "data:image/jpeg;base64," + imageData);
        },
        function (err) {
            console.log("Erreur Capture image : " + err.message);
            errorCB.call(this);
        },
        { quality: 50, destinationType: navigator.camera.DestinationType.DATA_URL }
        // qualité encodage 50%, format base64 (et JPEG par défaut)
    );
}
